(function() {
    'use strict';

    let $header, $progressBar, $body;

    window.addEventListener('load', () => {
        window.onscroll = () => {
            mainNavFixed();
            paintProgressBar();
            checkSidebars();
        };

        $progressBar = document.querySelector('.progress');
        $header = document.querySelector('.main-navbar');
        $body = document.querySelector('body');

        mainNavFixed();
        paintProgressBar();
        checkSidebars();

        // Get all user-interact elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger-toggle[data-target]'), 0);
        const $navbarPane = Array.prototype.slice.call(document.querySelectorAll('a.activate-pane'), 0);
        const $columnPane = Array.prototype.slice.call(document.querySelectorAll('a.activate-column-pane'), 0);
        const $tabItems = Array.prototype.slice.call(document.querySelectorAll('li.tab-item'), 0);
        const $closeTab = Array.prototype.slice.call(document.querySelectorAll('.close-tabs'), 0);
        const $closeColumnTab = Array.prototype.slice.call(document.querySelectorAll('.close-column-tabs'), 0);
        const $footerCategories = Array.prototype.slice.call(document.querySelectorAll('footer .toggle-list'), 0);

        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach( el => {
                el.addEventListener('click', () => navbarBurgerListener(el));
            });
        }

        // Check if there is any mobile devices content menu
        if ($navbarPane.length > 0) {

            // Add a click event on each of them
            $navbarPane.forEach( el => {
                el.addEventListener('click', () => navbarPaneListener(el));
            });
        }

        // Check if there is any sidebar column pane
        if ($columnPane.length > 0) {

            // Add a click event on each of them
            $columnPane.forEach( el => {
                el.addEventListener('click', () => columnPaneListener(el));
            });
        }

        // Check if there is any tab item (either for desktop or mobile devices)
        if ($tabItems.length > 0) {

            // Add a click event on each of them
            $tabItems.forEach( el => {
                el.addEventListener('click', () => tabItemsListener(el));
            });
        }

        // Check if there is any closeTab button
        if ($closeTab.length > 0) {

            // Add a click event on each of them
            $closeTab.forEach( el => {
                el.addEventListener('click', () => closeTabListener(el));
            });
        }

        // Check if there is any closeColumnTab button
        if ($closeColumnTab.length > 0) {

            // Add a click event on each of them
            $closeColumnTab.forEach( el => {
                el.addEventListener('click', () => closeColumnTabListener(el));
            });
        }

        // Check if there is any footer menu
        if ($footerCategories.length > 0) {

            // Add a click event on each of them
            $footerCategories.forEach( el => {
                el.addEventListener('click', () => footerCategoriesListener(el));
            });
        }
    });

    /**
     * Logic for opening/closing main menu in mobile devices
     * @param el
     */
    function navbarBurgerListener(el) {
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');

        if ($target) {
            $target.classList.toggle('is-active');
            $body.classList.toggle('is-main-navbar-open');

            if (target === 'mobile-top-navbar-menu') {
                toggleMobileMenu();
                //document.querySelector('.navbar-default-pane').classList.toggle('is-hidden');
            } else if (target === 'main-navbar-menu') {
                setNavbarMenuHeight(target);
            }

            addOverlay(target);
        }
    }

    /**
     * Logic for opening/closing content menu in mobile devices
     * @param el
     */
    function navbarPaneListener(el) {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $activeTarget = document.querySelector('.mobile-top-magic-burger-containers .navbar-container:not(.is-hidden)'),
            $activeTab = document.querySelector('.navbar-burger-toggle.is-active'),
            $activeTab2 = document.querySelector('.magic-row-item.is-active');

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        if ($target !== $activeTarget) {
            el.parentNode.classList.toggle('is-active');

            if ($target) {
                $target.classList.toggle('is-hidden');
            }
            if ($activeTarget) {
                $activeTarget.classList.toggle('is-hidden');
            }

            $body.classList.add('is-container-pane-open');

            setNavbarMenuHeight(target);
            addOverlay(target);
        } else {
            $body.classList.remove('is-container-pane-open');

            if ($target) {
                $target.classList.add('is-hidden');
            }

            closeOverlay();
        }

        if ($activeTab) {
            $activeTab.classList.remove('is-active');
        }

        if ($activeTab2) {
            $activeTab2.classList.remove('is-active');
        }
    }

    /**
     * Logic for opening/closing the right sidebar's pane
     * @param el
     */
    function columnPaneListener(el) {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $activeTarget = document.querySelector('.right-sidebar .navbar-container:not(.is-hidden)'),
            $activeTab = document.querySelector('.magic-column-item.is-active'),
            closestSidebar = el.closest('.right-sidebar');

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        if ($target !== $activeTarget) {
            el.parentNode.classList.toggle('is-active');

            if ($target) {
                $target.classList.toggle('is-hidden');
            }
            if ($activeTarget) {
                $activeTarget.classList.toggle('is-hidden');
            }

            $body.classList.add('is-container-pane-open');
            closestSidebar.classList.add('is-container-pane-open');

            setSidebarContainerPanePosition();
            setNavbarMenuHeight(target);
            addOverlay(target);
        } else {
            $body.classList.remove('is-container-pane-open');
            closestSidebar.classList.remove('is-container-pane-open');

            if ($target) {
                $target.classList.add('is-hidden');
            }

            closeOverlay();
        }

        if ($activeTab) {
            $activeTab.classList.remove('is-active');
        }
    }

    /**
     * Set the classes and visibility for mobile's content menu pane
     * @param el
     */
    function tabItemsListener(el) {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $targetTab = $target.parentElement.querySelector('.tab-item:not(.is-active)'),
            $activeTab = $target.parentElement.querySelector('.tab-item.is-active'),
            $activeTabContent = $target.parentElement.querySelector('.tab-content:not(.is-hidden)');

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $targetTab.classList.toggle('is-active');
        $target.classList.toggle('is-hidden');
        $activeTab.classList.toggle('is-active');
        $activeTabContent.classList.toggle('is-hidden');
    }

    /**
     * Close mobile's content menu pane
     * @param el
     */
    function closeTabListener(el) {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $activePane = document.querySelector('.magic-row-item.is-active');

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $target.classList.toggle('is-hidden');
        $body.classList.remove('is-container-pane-open');
        $activePane.classList.remove('is-active');

        closeOverlay();
    }

    /**
     * Closes the left sidebar's pane
     * @param el
     */
    function closeColumnTabListener(el) {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $activePane = document.querySelector('.magic-column-item.is-active');

        // Toggle the "is-hidden" class on $target
        $target.classList.toggle('is-hidden');

        // Remove classes from body, pane and sidebar
        document.body.classList.remove('is-column-container-pane-open');
        $activePane.classList.remove('is-active');
        el.closest('.right-sidebar').classList.remove('is-container-pane-open');

        closeOverlay();
    }

    /**
     * Changes visibility and icons for mobile's footer menu
     * @param el
     */
    function footerCategoriesListener(el) {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target,
            $target = document.querySelector(`.${target}`),
            $icon = el.querySelector('svg');

        // Toggle visibility
        $target.classList.toggle('is-hidden');

        // Toggle icons
        $icon.classList.toggle('fa-chevron-down');
        $icon.classList.toggle('fa-chevron-up');
    }

    /**
     * Sets the position of the right sidebar
     */
    function setSidebarContainerPanePosition() {
        const isProgressFixed = Array.prototype.indexOf.call($progressBar.classList, 'is-fixed-top'),
            $visiblePane = document.querySelector('.navbar-container-pane:not(.is-hidden)');
        let top = $progressBar.offsetTop + $progressBar.clientHeight;
        if (isProgressFixed > -1) {
            $visiblePane.classList.add('is-fixed-top');
            top = window.pageYOffset + $header.clientHeight + $progressBar.clientHeight;
        } else {
            $visiblePane.classList.remove('is-fixed-top');
        }
        $visiblePane.style.top = top + 'px';
    }

    /**
     * Sets the height of menu
     * @param navbar
     */
    function setNavbarMenuHeight(navbar) {
        const clientHeight = document.documentElement.clientHeight,
            $navbar = document.querySelector(`.${navbar}`),
            $navbarRect = $navbar.getBoundingClientRect();

        let height = 'initial';

        if (($navbarRect.top + $navbarRect.height) > clientHeight) {
            height = (clientHeight - $navbarRect.top) + 'px';
        }
        $navbar.style.height = height;
        $navbar.style.overflow = 'auto';
    }

    /**
     * Adds overlay to the DOM, sets the correct position based on the @param
     * @param from
     */
    function addOverlay(from) {
        if (from) {
            const width = document.body.scrollWidth;
            let top, height, $client, $overlay, $html;

            $client = document.querySelector(`.${from}`);
            $overlay = document.querySelector('.overlay');
            $html = document.querySelector('html');

            if (from === 'main-navbar-menu') {
                const clientRect = $client.getBoundingClientRect();
                top = clientRect.height + clientRect.top;
                height = document.body.clientHeight - clientRect.top;

                $overlay.classList.toggle('is-hidden');
                $html.classList.toggle('is-clipped');
            } else if (from.indexOf('-column-pane') > 0) {
                top = $client.offsetTop - 8;
                height = document.body.clientHeight - $client.offsetTop;

                $overlay.classList.remove('is-hidden');
                $html.classList.add('is-clipped');
            } else if (from.indexOf('navbar-') === 0) {
                top = $client.offsetHeight + $client.offsetTop;
                height = document.body.clientHeight - $client.offsetTop;

                $overlay.classList.remove('is-hidden');
                $html.classList.add('is-clipped');
            }

            $overlay.setAttribute('style',
                `top: ${top}px;height: ${height}px;width: ${width}px`
            );
        }
    }

    /**
     * Remove overlay from DOM
     */
    function closeOverlay() {
        document.querySelector('html').classList.remove('is-clipped');
        document.querySelector('.overlay').classList.add('is-hidden');
    }

    /**
     * Toggles mobile device top menu
     */
    function toggleMobileMenu() {
        document.querySelector('.navbar-burger.burger.logo').classList.toggle('is-hidden');
        document.querySelector('.navbar-burger.burger.login-icon').classList.toggle('is-hidden');
        document.querySelector('.navbar-burger.burger.magic-row').classList.toggle('is-hidden');
    }

    /**
     * Set value in progress bar based on scroll's position
     */
    function paintProgressBar() {
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop,
            height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        $progressBar.value =  (winScroll / height) * 100;
    }

    /**
     * Set sidebars position between upper and lower boundary
     */
    function checkSidebars() {
        const width = document.body.clientWidth;
        if (width >= 1088) {
            const $lowerBoundary = document.querySelector('.user-comments'),
                $rightSidebar = document.querySelector('.right-sidebar .magic-column'),
                $leftSidebar = document.querySelector('.left-sidebar .panel');

            if (window.pageYOffset > ($lowerBoundary.offsetTop - $leftSidebar.offsetHeight)) {
                $leftSidebar.style.top = window.pageYOffset + $leftSidebar.getBoundingClientRect().top + 'px';

                $leftSidebar.classList.remove('is-fixed-top');
                $leftSidebar.classList.add('is-absolute-top');
            } else {
                $leftSidebar.classList.remove('is-absolute-top');
                $leftSidebar.classList.add('is-fixed-top');

                $leftSidebar.style.top = 'initial';
            }

            if (window.pageYOffset > ($lowerBoundary.offsetTop - $rightSidebar.offsetHeight)) {
                $rightSidebar.style.top = window.pageYOffset + $rightSidebar.getBoundingClientRect().top + 'px';

                $rightSidebar.classList.remove('is-fixed-top');
                $rightSidebar.classList.add('is-absolute-top');
            } else {
                $rightSidebar.classList.remove('is-absolute-top');
                $rightSidebar.classList.add('is-fixed-top');

                $rightSidebar.style.top = 'initial';
            }
        }

    }

    /**
     * Set/unset main menu's fixed position
     */
    function mainNavFixed() {
        const sticky = $header.offsetTop;
        if (window.pageYOffset > sticky) {
            $header.classList.add('is-fixed-top');
            $progressBar.classList.add('is-fixed-top');
        } else {
            $header.classList.remove('is-fixed-top');
            $progressBar.classList.remove('is-fixed-top');
        }
    }
})();